# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Clientes(models.Model):
    _name = 'inscrito'

    avatar = fields.Binary()
    rut = fields.Char(string='Rut')
    nombre = fields.Char(string='Nombre')
    apellido_materno = fields.Char(string='Apellido Materno')
    apellido_paterno = fields.Char(string='Apellido Paterno')
    direccion = fields.Char(string='Dirección')
    telefono = fields.Char(string='Teléfono')
    movil = fields.Char(string='Móvil')
    email = fields.Char(string='Email')
    cesfam = fields.Selection([('cesfam_hernan_urzua', 'CESFAM H.URZUA'),('cesfam_bicentenario', 'CESFAM BICENTENARIO'),('cesfam_renca', 'CESFAM RENCA'),('cesfam_huamachuco', 'CESFAM HUAMACHUCO')])
    descripcion_medicamento_1 = fields.Char(string='Descripcion_1')
    descripcion_medicamento_2 = fields.Char(string='Descripcion_2')
    descripcion_medicamento_3 = fields.Char(string='Descripcion_3')
    descripcion_medicamento_4 = fields.Char(string='Descripcion_4')
    descripcion_medicamento_5 = fields.Char(string='Descripcion_5')
    descripcion_medicamento_6 = fields.Char(string='Descripcion_6')
    descripcion_medicamento_7 = fields.Char(string='Descripcion_7')
    descripcion_medicamento_8 = fields.Char(string='Descripcion_8')
    descripcion_medicamento_9 = fields.Char(string='Descripcion_9')
    descripcion_medicamento_10 = fields.Char(string='Descripcion_10')
    documentos_ids = fields.Many2many('documentos.requeridos')
    edad = fields.Integer(string='Edad')
    estado_despacho = fields.Selection([('por_confirmar','Por Confirmar'),('con_despacho','Con Despacho')])
    estado_documentacion = fields.Selection([('aprobado','Aprobado'),('en_proceso','En Proceso'),('rechazado','Rechazado')])
    fecha_expiracion_receta = fields.Date(string='Fecha Expiración')
    fecha_nacimiento = fields.Date(string='Fecha de Nacimiento')
    fecha_prescripcion = fields.Datetime(string='Fecha Prescripción')
    genero = fields.Selection([('male', 'Masculino'),('female', 'Femenino')])
    nacionalidad = fields.Selection([('argentina','Argentina'), ('bolivia','Boliviana'), ('brasil','Brasileña'), ('chile','Chilena'), ('colombia','Colombiana'), ('ecuador','Ecuatoriana'), ('guayana','Guyanesa'), ('paraguay','Paraguaya'), ('peru','Peruana'), ('suriname','Surinamesa'), ('uruguay','Uruguaya'), ('venezuela','Venezolana'), ('haiti','Haitiana'),('europa','Europea'), ('alemania','Alemana'),('croacia','Croata'), ('belgica','Belga'),('gitana','Gitana')] )
    poblacion = fields.Selection([('A','ADRIANA PACHECO'), ('B','ALFREDO FREITTE'), ('C','ALTUE NORTE'), ('D','ALTUÉ SUR'), ('E','AMANCAY'), ('F','AMERICO VESPUCIO'), ('G','ANGAMOS'), ('H','ANIBAL PINTO(EX LUTHER KING)'), ('I','ANTUMALAL'), ('J','APOST'), ('K','ARMANDO ARTIGAS'), ('L','ARTURO PRAT'), ('M','ASTRA'), ('N','BALMACEDA PONIENTE'), ('O','BALMACEDA PONIENTE (Casas)'), ('P','BARRIO FUNDADORES'), ('Q','BARRIO GIRASOL'), ('R','BLANCA VERGARA'), ('S','BRASIL(LOTEO)'), ('T','BULNES'), ('U','C.C.U.(CASAS)'), ('V','C.C.U.(S.)'), ('U','CALVO MACKENNA'), ('W','CASAS DE RENCA 1'), ('X','CASAS DE RENCA 2'), ('Y','CASAS DE RENCA 3'), ('Z','CASAS DE RENCA 4'), ('AA','CASAS JARDIN DE RENCA'), ('A1','CAUPOLICAN EMPLEADOS'), ('A2','CAUPOLICAN OBRERO'), ('A3','CERRO COLORADO'), ('A4','CIUDAD NUEVA RENCA'), ('A5','COND ALTO MIRAFLORES I'), ('A6','COND ALTO MIRAFLORES II'), ('A7','COND EL MAGNOLIO (Ex-Hacienda)'), ('A8','COND. DON JOSE MANUEL'), ('A9','CONDOM LOS SILOS'), ('B1','CONDOM NTO SR DE RENCA'), ('B2','CONDOM. NUEVA RENCA IV'), ('B3','CONDOMIN LAS ARAUCARIAS(Dep.)'), ('B4','CONDOMINIO LOS ALERCES(Dep.)'), ('B5','CONDOMINIO LOS CANELOS(Dep.)'), ('B6','CONDOMINIO LOS PEUMOS(Dep.)'), ('B7','CONJ. HABIT JARDIN PONIENTE'), ('B8','CONJ. HABIT. PEDRO OÑA'), ('B9','CONJ.HABIT.LO BOZA'), ('B10','CONJ.HABITAC.MIRAFLORES'), ('C1','COOPERATIVA ACMA'), ('C2','COOPERATIVA MAGALLANES'), ('C3','CORDERO MONTANA'), ('C4','CORFO'), ('C5','DAVANZO(LOTEO)'), ('C6','DE LA CRUZ'), ('C7','DGO. PERRIER'), ('C8','DIEGO PORTALES'), ('C9','DOMINGO SANTA MARIA'), ('D1','E.ILLANES BEYTIA'), ('D2','EBEN EZER'), ('D3','EL COBRE'), ('D4','EL CORTIJO'), ('D5','EL DAMASCAL'), ('D6','EL ESFUERZO'), ('D7','EL NOGAL'), ('D8','EL PIAMONT'), ('D9','EL SALVADOR'), ('D10','EL SALVADOR(CASAS)'), ('E1','EL TENIENT'), ('E2','EL TRIGAL'), ('E3','EMPLEADOS HIRMAS'), ('E4','ESMERALDA (VILLA)'), ('E5','FERNANDO DE LA MAZA'), ('E6','FERRILOZA'), ('E7','FRANCISCO INFANTE'), ('E8','GABRIELA MISTRAL'), ('E9','GENERAL FREIRE'), ('E10','GENERAL VERGARA'), ('F1','HERIBERTO RAMIREZ'), ('F2','HIRMAS II'), ('F3','HORACIO POLANCO'), ('F4','HUAMACHUCO 1(EX 1º DE MAYO)'), ('F5','HUAMACHUCO 2'), ('F6','HUAMACHUCO 2CASAS'), ('F7','HUAMACHUCO 3(Ex Fresia)'), ('F8','HUAMACHUCO I(EX JOSE CARDIN)'), ('F9','INES DE SUAREZ'), ('F10','ISABEL BRICEÑO'), ('G1','ISLA DE CHILOE'), ('G2','J.KENNEDY'), ('G3','JARDIN PONIENET 2 (s) a 05.17'), ('G4','JARDIN PONIENTE'), ('G5','JARDIN PONIENTE 2'), ('G6','JARDIN PONIENTE 2(CASAS)'), ('G7','JORGE PRAT'), ('G8','JOSE MIGUEL CARRERA'), ('G9','JOSE MIGUEL INFANTE 1-2'), ('G10','JUAN CORDERO'), ('H1','JUVENTUD RENCA'), ('H2','LA ALBORAD'), ('H3','LA ARAUCANIA'), ('H4','LA JAVA'), ('H5','LA MONTAÑA'), ('H6','LA POLLITA'), ('H7','LA PONDEROSA'), ('H8','LA QUEBRADA'), ('H9','LA VIÑITA'), ('H10','LAS AÑAÑUCAS'), ('I1','LAS CAMELIAS 1-2'), ('I2','LAS CUNCUNAS'), ('I3','LAS GLORIETAS'), ('I4','LAS HIGUERAS'), ('I5','LAS LILAS 1'), ('I6','LAS LILAS 2'), ('I7','LAS MARGARITAS 1(s.)'), ('I8','LAS MARGARITAS 2(s.)'), ('I9','LAS MARGARITAS 3(s.)'), ('110','LAURA VICUÑA(Casas)'), ('J1','LO BENITO'), ('J2','LO NEGRETE'), ('J3','LO VELASQUEZ 1'), ('J4','LO VELASQUEZ 2'), ('J5','LO VELASQUEZ 3'), ('J6','LO VELASQUEZ 4'), ('J7','LO VELASQUEZ 5'), ('J8','LO VELASQUEZ 6'), ('J9','LOS CISNES'), ('J10','LOS COPIHUES'), ('J11','LOS JARDINES DE DON ANIBAL'), ('J12','LOS NARANJOS'), ('J13','LOTEO JULIO BASCUR'), ('J14','LOTEO LOS ROSALES(Ex-Hacienda)'), ('K1','LOTEO MADRID'), ('K2','LOTEO NAHMIAS'), ('K3','LOTEO TADEO GUTIERREZ'), ('K4','LOURDES'), ('K5','MADRID STGO'), ('K6','MANUEL RODRIGUEZ'), ('K7','MATUCANA'), ('K8','MAULE 1'), ('K9','MAULE 2'), ('K10','MAULE 3'), ('L1','MERCEDES FERNANDEZ'), ('L2','MIRAFLORES MAYA'), ('L3','MUJER DE ESFUERZO'), ('L4','NORTE CHICO'), ('L5','NORTE GRANDE'), ('L6','NUESTROS SUEÑOS'), ('L7','NUEVA VIDA'), ('L8','OBREROS HIRMAS'), ('L9','OSCAR CASTRO'), ('L10','PABLO RODRIGUEZ'), ('M1','PARQUE BALMACEDA'), ('M2','PEDRO AGUIRRE CERDA'), ('M3','PUEBLA FRIAS'), ('M4','RAFAEL NEGRETE'), ('M5','RENACER'), ('M6','RENCA ANTIGUO'), ('M7','RENCA LTDA'), ('M8','RIO GRANDE'), ('M9','ROBINSON ROJAS'), ('M10','ROQUER BRAVO'), ('N1','RUCAHUE (s.)'), ('N2','SAN CARLOS'), ('N3','SAN BENILD'), ('N4','SAN GENARO'), ('N5','SAN LUIS'), ('N6','SAN MAURICIO'), ('N7','SANTA ANA'), ('N8','SANTA BARBARA'), ('N9','SANTA CRUZ DE LO BOZA'), ('N10','SANTA EMILIA'), ('O1','SANTA ROSA'), ('O2','SARMIENTO'), ('O3','SERVANDO AGUILERA'), ('O4','SIG. BASCUÑAN'), ('O5','SOL DE ANGAMOS'), ('O6','SOL DE ANGAMOS (Casas)'), ('O7','SOL DE FREIRE I'), ('O8','TRANVIARIO'), ('O9','TUCAPEL JIMENEZ I'), ('O10','TUCAPEL JIMENEZ II'), ('P1','VALENZUELA PUELMA'), ('P2','VALLE AUSTRAL'), ('P3','VALLE CENTRAL'), ('P4','VALLE DE AZAPA sector A1-A6'), ('P5','VALLE DE AZAPA (Casas) sector A1-A6'), ('P6','VALLE DE AZAPA sector A4-A3'), ('P7','VALLE DE AZAPA sector A4-A3(Casas)'), ('P8','VALLE DE AZAPA sector A5-A7'), ('P9','VALLE DE AZAPA sector A5-A7(Casas)'), ('P10','VALLE DEL ELQUI'), ('Q1','VICKY BARAHONA 1'), ('Q2','VICTORIA'), ('Q3','VILLA BALMACEDA'), ('Q4','VILLA CAUPOLICAN'), ('Q5','VILLA COSTANERA'), ('Q6','VILLA EL SOL'), ('Q7','VILLA ESPAÑA'), ('Q8','VILLA ESPERANZA'), ('Q9','VILLA FUTURO'), ('Q10','VILLA GESTION'), ('R1','VILLA HERMOSA'), ('R2','VILLA HORIZONTE'), ('R3','VILLA LAS MERCEDES'), ('R4','VILLA MAFIL'), ('R5','VILLA O´HIGGINS'), ('R6','VILLA PARROQUIAL'), ('R7','VILLA UNID'), ('R8','VIÑA DEL MAR'), ('R9','VOZ DEL DESIERTO') ])
    prevision = fields.Selection([('fonasa','Fonasa'),('dipreca','Dipreca'),('isapre', 'Isapre'),('capredena','Capredena'),('otros','Otros')])
    cedula_identidad = fields.Binary(string="Cedula de Identidad o Pasaporte")
    certificado_residencia = fields.Binary(string="Certificado de Residencia")
    receta_medica = fields.Binary(string="Receta medica")
