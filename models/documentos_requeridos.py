# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Documentos(models.Model):
    _name = 'documentos.requeridos'



    nombre_documento = fields.Char(string='Nombre Documento')
    activo = fields.Boolean(string="Activo", default=False)